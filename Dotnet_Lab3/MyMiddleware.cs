﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dotnet_Lab3
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class StaticFileMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public StaticFileMiddleware(RequestDelegate next, ILoggerFactory logger)
        {
            _logger = logger.CreateLogger("StaticFileMiddleware");

             _next = next;  
        }

        public async Task Invoke(HttpContext httpContext)
        {
            _logger.LogInformation("StaticFileMiddleware executing..");

            await _next(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class StaticFileMiddlewareExtensions
    {
        public static IApplicationBuilder UseStaticFileMiddleware(this IApplicationBuilder builder)
        {
            //DefaultFilesOptions defaultFilesOptions = new DefaultFilesOptions();
            //defaultFilesOptions.DefaultFileNames.Clear();
            //defaultFilesOptions.DefaultFileNames.Add("htmlpage.html");

            return builder.UseDefaultFiles()
                .UseStaticFiles()
                .UseMiddleware<StaticFileMiddleware>();
        }
    }
}
